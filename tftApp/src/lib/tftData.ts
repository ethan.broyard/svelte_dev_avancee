// src/lib/tftData.ts

export interface Champion {
    name: string;
    description: string;
    image: string;
    familyId: string; // Clé étrangère référençant l'ID de la famille
  }
  
  export interface Family {
    id: string;
    name: string;
    description: string;
    image: string;
  }
  
export const families = [
    {
        id: 'sorcerer',
        name: "Sorcerer",
        description: "Les sorciés utilisent la lumière pour révéler leur véritable potentiel, augmentant leur puissance magique",
        image: "/images/families/sorcerer.jpg",
      },
      {
        id: 'demacia',
        name: "Demacia",
        description: "Les Seigneurs de guerre gagnent en puissance à mesure qu'ils remportent des batailles, renforçant leur armée.",
        image: "/images/families/demacia.jpg",
      },
      {
        id: 'void',
        name: "Void",
        description: "Obtenez un œuf du Néant posable. Au début du combat, il éclot en une horreur innommable et étourdit les ennemis adjacents. Chaque niveau d’étoile des champions du Néant augmente la santé et la puissance de l’horreur de 25 %. ",
        image: "/images/families/void.jpg",
      },
       {
        id: 'ionia',
        name: "Ionia",
        description: "Toutes les 4 secondes, vos Ioniens les plus forts prennent leur forme illuminée et gagnent 20 Mana. Chaque Ionien a un bonus unique dans sa capacité, qui est doublé lorsqu’il est sous forme illuminée.",
        image: "/images/families/ionia.jpg",
      },
      {
        id: 'freljord',
        name: "Freljord",
        description: "Au bout de 5 secondes, une tempête de glace s’abat sur le champ de bataille. Les ennemis subissent un pourcentage de leur maximum de points de vie sous forme de dégâts purs et ont des affaiblissements pendant 15 secondes.",
        image: "/images/families/freljord.jpg",
      },
      {
        id: 'noxus',
        name: "Noxus",
        description: "Les unités de Noxus gagnent des points de vie, de la puissance et des dégâts d’attaque. Ce gain est augmenté de 10 % pour chaque adversaire différent que vous avez vaincu en combat ou qui est mort.",
        image: "/images/families/noxus.jpg",
      },
    // Ajoutez d'autres familles, descriptions, et champions comme nécessaire
  ];
  
export const champions: Champion[] = [
    {
      name: "Lux",
      description: "Light mage with stunning abilities.",
      image: "/images/champions/Lux.jpeg",
      familyId: 'sorcerer',
    },
    {
      name: "Garen",
      description: "Valiant warrior with spinning attack.",
      image: "/images/champions/Garen.jpeg",
      familyId: 'demacia',
    },
    {
        name:"Kha'Zix",
        description: "Utilise ses griffes acérées pour infliger des dégâts critiques aux ennemis isolés.",
        image: "/images/champions/Malzahar.jpeg",
        familyId: 'void',
    },
    {
        name:"Cho'Gath",
        description: "Dévore les ennemis, gagnant des bonus permanents à sa santé pour chaque ennemi consommé.",
        image: "/images/champions/ChoGath.jpeg",
        familyId: 'void'

    },
    {
        name:"Yasuo",
        description: "Déchaîne un tourbillon d'épée, tranchant les ennemis en ligne droite.",
        image: "/images/champions/Yasuo.jpeg",
        familyId: 'ionia'
    },
    {
        name:"Kennen",
        description: "Invoque une tempête électrique qui étourdit les ennemis après plusieurs impacts.",
        image: "/images/champions/Kennen.jpeg",
        familyId: 'ionia'
    },
    {
        name: "Ashe",
        description: "Tire une flèche de glace qui étourdit et inflige des dégâts au premier ennemi touché, avec la distance augmentant l'effet de l'étourdissement.",
        image: "/images/champions/Ashe.jpeg",
        familyId: 'freljord',
      },
      {
        name: "Sejuani",
        description: "Charge et crée une explosion de gel au point d'impact, étourdissant les ennemis proches et infligeant des dégâts.",
        image: "/images/champions/Sejuani.jpeg",
        familyId: 'freljord',
      },
      {
        name: "Darius",
        description: "Effectue un coup de hache circulaire, infligeant des dégâts à tous les ennemis autour de lui et se soignant pour chaque ennemi touché.",
        image: "/images/champions/Darius.jpeg",
        familyId: 'noxus',
      },
      {
        name: "Katarina",
        description: "Lance un déluge de lames sur une courte durée, infligeant des dégâts aux ennemis proches et ayant une chance de réduire le temps de recharge de ses compétences.",
        image: "/images/champions/Katarina.jpeg",
        familyId: 'noxus',
      }
      
      
    // Autres champions...
  ];

  export const arenas = [
    {
      id: 1,
      name: "Default",
      imageUrl: "/images/arenas/Default.jpg",
    },
    {
      id: 2,
      name: "Dark Star",
      imageUrl: "/images/arenas/DarkHole.jpg",
    },
    {
        id: 3,
        name: "Elemental",
        imageUrl: "/images/arenas/Infernal.jpg",
    },
    {
        id: 4,
        name: "Supernova",
        imageUrl: "/images/arenas/Supernova.jpg",
    },
    {
        id: 5,
        name: "Freljord Avarosa",
        imageUrl: "/images/arenas/Avarosa.jpg",
    },
    {
        id: 6,
        name: "Freljord Frostguard",
        imageUrl: "/images/arenas/Frostguard.jpg",
    },
    {
        id: 7,
        name: "Freljord Winter",
        imageUrl: "/images/arenas/Winter.jpg",
    },
    {
        id: 8,
        name: "Galaxy",
        imageUrl: "/images/arenas/PitStop.jpg",
    },
    {
        id: 9,
        name: "Gadgets jinx",
        imageUrl: "/images/arenas/Jinx.jpg",
    },
    {
        id: 10,
        name: "Gadgets Vi",
        imageUrl: "/images/arenas/Vi.jpg",
    },
    // Ajoutez d'autres arènes ici...
  ];